<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title>ImageUploader</title>
  <link rel="stylesheet" href="./style.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
    crossorigin="anonymous" />
</head>

<body>
      <?php 
        if(isset($_POST['upload'])){
          $t=time();
          $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
          $name = time();
          $file_type = $_FILES['file']['type'];
          $file_size = $_FILES['file']['size'];
          $file_tem_loc = $_FILES['file']['tmp_name'];
          $file_store = "public/images/".$name.".".$extension;
          
          move_uploaded_file($file_tem_loc, $file_store);
          $files = scandir('public/images/');
          foreach($files as $file) {

            $tmp = "public/images/$file";
             echo "<a href='delete-image.php?img=$tmp'><img src='public/images/".$file."'  
             width='20%'></a>";
          }
        }
    ?>

        <form enctype="multipart/form-data" action="" method="POST">
            <label>Uploading File</label>
            <p><input type="file" name="file"/></p>
            <p><input type="submit" name="upload" value="Upload Image"></p>
	      </form> 
</body>

</html>
